﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scaffold.Models.Interface
{
    public interface IDatedEntity
    {
        DateTime CreatedAt { get; set; }
        DateTime? LastUpdateTime { get; set; }
    }
}
