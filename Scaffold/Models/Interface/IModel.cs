﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scaffold.Models.Interface
{
    public interface IModel<TId> : IDatedEntity
    {
        TId Id { get; set; }
    }
}
