﻿namespace Scaffold.Models.Interface
{
    public interface ISoftDeletable
    {
        bool IsDeleted { get; set; } 
    }
}