﻿//-----------------------------------------------------------------------
// <copyright file="MapCoordinates.cs" company="PT. Sangkuriang Internasional">
//     Copyright (c) PT. Sangkuriang Internasional. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Model
{
    public class DecimalLocation
    {
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }

        public override string ToString()
        {
            return string.Format("{0:f5}, {1:f5}",
                Latitude, Longitude);
        }
    }

    public class DmsLocation
    {
        public DmsPoint Latitude { get; set; }
        public DmsPoint Longitude { get; set; }

        public override string ToString()
        {
            return string.Format("{0}, {1}",
                Latitude, Longitude);
        }
    }

    public class DmsPoint
    {
        public int Degrees { get; set; }
        public int Minutes { get; set; }
        public int Seconds { get; set; }
        public PointType Type { get; set; }

        public override string ToString()
        {
            return string.Format("{0} {1} {2} {3}",
                Math.Abs(Degrees),
                Minutes,
                Seconds,
                Type == PointType.Lat
                    ? Degrees < 0 ? "S" : "N"
                    : Degrees < 0 ? "W" : "E");
        }
    }

    public enum PointType
    {
        Lat,
        Lon
    }
}
