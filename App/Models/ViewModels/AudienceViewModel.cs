﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using App.Infrastructure;
using AutoMapper;
using Heroic.AutoMapper;

namespace App.Models.ViewModels
{
    public class AudienceViewModel : IMapFrom<Audience>, IHaveCustomMappings
    {
        public string ClientId { get; set; }
        [Required]
        public string Base64Secret { get; set; }
        [Required]
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? LastUpdateTime { get; set; }
        
        public void CreateMappings(IConfiguration configuration)
        {
            configuration.CreateMap<Audience, AudienceViewModel>();
        }

        public virtual Audience ToEntity()
        {
            return new Audience()
            {
                ClientId = ClientId,
                Base64Secret = Base64Secret,
                Name = Name,
                CreatedAt = CreatedAt,
                LastUpdateTime = LastUpdateTime,
            };
        }
    }
}

