﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data.SqlClient;
using System.Linq;
using App.Infrastructure;
using Domain.Infrastructure;
using Domain.Models;
using Scaffold.Models.Interface;
using TrackerEnabledDbContext.Common;
using TrackerEnabledDbContext.Common.Models;
using TrackerEnabledDbContext.Identity;

namespace App.DAL
{
    public partial class ApplicationDbContext : TrackerIdentityContext<ApplicationUser>
    {
        public ApplicationDbContext() : base("DefaultConnection", throwIfV1Schema: false) {}
        public IDbSet<UserProfile> UserProfiles { get; set; }
        public IDbSet<Audience> Audiences { get; set; }
        public IDbSet<Permission> Permissions { get; set; }
        public IDbSet<RolePermission> RolePermissions { get; set; }
        public IDbSet<Menu> Menus { get; set; }
        public IDbSet<RoleMenu> RoleMenus { get; set; }
        public IDbSet<Category> Categories { get; set; }
        public IDbSet<Product> Products { get; set; }
        public IDbSet<Ticket> Tickets { get; set; }
        public IDbSet<TicketComment> TicketComments { get; set; }
        public IDbSet<Solution> Solutions { get; set; }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public override int SaveChanges(object userName)
        {
            var changed = ChangeTracker.Entries();

            if (changed != null)
            {
                foreach (var entry in changed.Where(e => e.State == EntityState.Deleted || e.Entity is IDatedEntity))
                {
                    if (entry.Entity is ISoftDeletable && entry.State == EntityState.Deleted)
                    {
                        using (var auditer = new LogAuditor(entry))
                        {
                            var record = auditer.CreateLogRecord(userName, EventType.Deleted, this);
                            if (record != null)
                            {
                                this.AuditLog.Add(record);
                            }
                        }
                        SoftDelete(entry);
                    }

                    if (entry.Entity is IDatedEntity)
                    {
                        var entity = entry.Entity as IDatedEntity;
                        switch (entry.State)
                        {
                            case EntityState.Added:
                                entity.CreatedAt = DateTime.UtcNow;
                                entity.LastUpdateTime = entity.CreatedAt;
                                break;
                            case EntityState.Modified:
                                entity.LastUpdateTime = DateTime.UtcNow;
                                break;
                        }
                    }
                }
            }

            return base.SaveChanges(userName);
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Entity<Category>().Map(m => m.Requires("IsDeleted").HasValue(false)).Ignore(m => m.IsDeleted);
            modelBuilder.Entity<Product>().Map(m => m.Requires("IsDeleted").HasValue(false)).Ignore(m => m.IsDeleted);

            modelBuilder.Entity<Ticket>().Map(m => m.Requires("IsDeleted").HasValue(false)).Ignore(m => m.IsDeleted);
            modelBuilder.Entity<TicketComment>().Map(m => m.Requires("IsDeleted").HasValue(false)).Ignore(m => m.IsDeleted);
            modelBuilder.Entity<Solution>().Map(m => m.Requires("IsDeleted").HasValue(false)).Ignore(m => m.IsDeleted);
 

            base.OnModelCreating(modelBuilder);
        }

        private void SoftDelete(DbEntityEntry entry)
        {
            var entryEntityType = entry.Entity.GetType();

            var tableName = GetTableName(entryEntityType);
            var primaryKeyName = GetPrimaryKeyName(entryEntityType);

            // Un-Comment below to change to SQL Server
            var sql = string.Format("UPDATE {0} SET IsDeleted = 1 WHERE {1} = @id", tableName, primaryKeyName);

            Database.ExecuteSqlCommand(sql, new SqlParameter("@id", entry.OriginalValues[primaryKeyName]));

            // Comment below to change to SQL Server
            //var sql = string.Format("UPDATE \"{0}\" SET \"IsDeleted\" = 't' WHERE \"{1}\" = @id", tableName.Replace("[dbo].[", "").Replace("]", ""), primaryKeyName);

            //Database.ExecuteSqlCommand(TransactionalBehavior.EnsureTransaction, sql, new Npgsql.NpgsqlParameter("@id", entry.OriginalValues[primaryKeyName]));

            // prevent hard delete
            entry.State = EntityState.Detached;
        }

        public System.Data.Entity.DbSet<Domain.Infrastructure.ApplicationRole> IdentityRoles { get; set; }

        //public System.Data.Entity.DbSet<Domain.Infrastructure.ApplicationUser> ApplicationUsers { get; set; }
    }
}