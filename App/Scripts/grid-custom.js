﻿function gridDataBound(e) {
    var grid = e.sender;
    if (grid.dataSource.total() == 0) {
        var colCount = grid.columns.length;
        var msg = "Currently no data available";

        if (grid.options.noDataMessage !== undefined) {
            msg = grid.options.noDataMessage;
        }

        $(e.sender.wrapper)
            .find("tbody")
            .append("<tr class=\"kendo-data-row\"><td colspan=\"" + colCount + "\" class=\"no-data\">" + msg + "</td></tr>");
    }

    $("span.k-icon.k-delete").removeClass("k-icon k-delete");
    $("span.k-icon.k-i-excel").removeClass("k-icon k-i-excel");
    $("span.k-icon.k-edit").removeClass("k-icon k-edit");
};

function gridEdit(e) {
    //Triggered when the window is closed (always)
    e.container.data("kendoWindow").bind("deactivate", function () {
        $(e.sender.wrapper).data("kendoGrid").dataSource.read();
    });

    //Triggered when a Window is closed (by a user or through the close() method)
    e.container.data("kendoWindow").bind("close", function () {
        $(e.sender.wrapper).data("kendoGrid").dataSource.read();
    });
};

function gridMultiSelectFilter(element, api, fieldId, value, text, isGuid) {
    element.removeAttr("data-bind");

    element.kendoMultiSelect({
        dataSource: {
            type: "json",
            transport: {
                read: {
                    url: config.baseUrl + "api/" + api,
                    dataType: "json",
                    contentType: "application/json",
                },
                parameterMap: function (options, operation) {
                    if (operation === "destroy") {
                        return null;
                    } else if (operation !== "read" && options.models) {
                        return { models: kendo.stringify(options.models) };
                    } else {
                        return "q=" + JSON.stringify(options);
                    }
                }
            },
            schema: {
                data: "Data",
                total: "Total",
                model: {
                    id: 'Id',
                    fields: {

                    }
                }
            },
            serverFiltering: true,
            serverPaging: true,
            pageSize: 7
        },
        change: function (e) {
            var filter = { logic: "or", filters: [] };
            var values = this.value();
            $.each(values, function (i, v) {
                filter.filters.push({ field: fieldId, operator: "eq", value: isGuid ? "guid'" + v + "'" : v });
            });
            dataSource.filter(filter);
        },
        dataTextField: text,
        dataValueField: value
    });
}