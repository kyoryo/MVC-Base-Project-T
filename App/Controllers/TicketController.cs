﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Domain.Models;
using App.DAL;
using App.Enums;
using Microsoft.AspNet.Identity;

namespace App.Controllers
{
    public class TicketEventArgs : EventArgs
    {
        public readonly Ticket After;
        public readonly CrudEventEnumStatus Status;

        public TicketEventArgs(Ticket after, CrudEventEnumStatus status)
        {
            this.After = after;
            this.Status = status;
        }
    }

    public class TicketController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public static event EventHandler<TicketEventArgs> AfterCall;

        protected virtual void OnCrudOperation(TicketEventArgs e)
        {
            if (AfterCall != null)
            {
                AfterCall(this, e);
            }
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ticket ticket = db.Tickets.Find(id);
            ViewBag.Id = id;
            if (ticket == null)
            {
                return HttpNotFound();
            }
            return View(ticket);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Ticket ticket)
        {
            if (ModelState.IsValid)
            {
                ticket.CreatorId = User.Identity.GetUserId();
                db.Tickets.Add(ticket);

                db.SaveChanges(User.Identity.GetUserName());

                OnCrudOperation(new TicketEventArgs(ticket, CrudEventEnumStatus.Create));

                if (!string.IsNullOrWhiteSpace(Request.QueryString["createNext"]))
                {
                    if (Request.QueryString["createNext"] == "true")
                    {
                        TempData["Created"] = true;
                        TempData["Alert"] = "Berhasil menambahkan Ticket";
                        TempData["AlertClass"] = "success";

                        return RedirectToAction("Create");
                    }
                }

                return RedirectToAction("Details", new { id = ticket.Id});
            }

            TempData["Alert"] = "Gagal menambahkan Ticket. Harap periksa kembali data yang anda input.";
            TempData["AlertClass"] = "danger";

            return View(ticket);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ticket ticket = db.Tickets.Find(id);
            ViewBag.Id = id;

            if (ticket == null)
            {
                return HttpNotFound();
            }

            return View(ticket);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Ticket ticket)
        {
            if (ModelState.IsValid)
            {
                db.Entry(ticket).State = EntityState.Modified;

                db.SaveChanges(User.Identity.GetUserName());

                OnCrudOperation(new TicketEventArgs(ticket, CrudEventEnumStatus.Update));

                TempData["Alert"] = "Berhasil menyimpan perubahan Ticket";
                TempData["AlertClass"] = "success";

                return RedirectToAction("Details", new { id = ticket.Id});
            }
            ViewBag.Id = ticket.Id;

            TempData["Alert"] = "Gagal melakukan perubahan Ticket. Harap periksa kembali data yang anda masukkan.";
            TempData["AlertClass"] = "danger";

            return View(ticket);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Ticket ticket = db.Tickets.Find(id);
            ViewBag.Id = id;

            if (ticket == null)
            {
                return HttpNotFound();
            }
            return View(ticket);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Ticket ticket = db.Tickets.Find(id);
            ViewBag.Id = id;
            db.Tickets.Remove(ticket);
            db.SaveChanges(User.Identity.GetUserName());
            OnCrudOperation(new TicketEventArgs(ticket, CrudEventEnumStatus.Delete));
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
