﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Kendo.DynamicLinq;
using Newtonsoft.Json;
using App.Infrastructure;
using App.DAL;
using App.Enums;
using App.Helper;
using App.Models.ViewModels;
using Microsoft.AspNet.Identity;

namespace App.Controllers.WebApi
{
    public class AudiencesControllerEventArgs : EventArgs
    {
        public readonly Audience After;
        public readonly CrudEventEnumStatus Status;

        public AudiencesControllerEventArgs(Audience after, CrudEventEnumStatus status)
        {
            this.After = after;
            this.Status = status;
        }
    }

    public class AudiencesController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public static event EventHandler<AudiencesControllerEventArgs> AfterCall;

        protected virtual void OnCrudOperation(AudiencesControllerEventArgs e)
        {
            if (AfterCall != null)
            {
                AfterCall(this, e);
            }
        }

        // GET: api/Audiences
        public DataSourceResult GetAudiences(HttpRequestMessage requestMessage)
        {
            DataSourceRequest request =
                JsonConvert.DeserializeObject<DataSourceRequest>(requestMessage.RequestUri.ParseQueryString().GetValues("q")[0]);

            if (request.Filter != null && !request.Filter.Filters.Any())
            {
                request.Filter = null;
            }
            else
            {
                request.Filter.ToCustomExpression();
            }

            var result = db.Audiences.AsQueryable();

            return result
                .OrderBy(x => x.Name)
                .ToDataSourceResult(request.Take, request.Skip, request.Sort, request.Filter);
        }

        // GET: api/Audiences/5
        [ResponseType(typeof(Audience))]
        public IHttpActionResult GetAudience(string id)
        {
            Audience audience = db.Audiences.Find(id);
            if (audience == null)
            {
                return NotFound();
            }

            return Ok(audience);
        }

        // PUT: api/Audiences/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutAudience(string id, AudienceViewModel audience)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != audience.ClientId)
            {
                return BadRequest();
            }

            var temp = AudiencesStore.FindAudience(id);
            temp.Name = audience.Name;
            db.Entry(temp).State = EntityState.Modified;

            try
            {
                db.SaveChanges(User.Identity.GetUserName());
                OnCrudOperation(new AudiencesControllerEventArgs(temp, CrudEventEnumStatus.Update));
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AudienceExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Audiences
        [ResponseType(typeof(Audience))]
        public IHttpActionResult PostAudience(AudienceViewModel audience)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var tempaudience = audience.ToEntity();

            try
            {
                var result = AudiencesStore.AddAudience(audience.Name);
                OnCrudOperation(new AudiencesControllerEventArgs(result, CrudEventEnumStatus.Create));
            }
            catch (DbUpdateException)
            {
                if (AudienceExists(audience.ClientId))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = audience.ClientId }, tempaudience);
        }

        // DELETE: api/Audiences/5
        [ResponseType(typeof(Audience))]
        public IHttpActionResult DeleteAudience(string id)
        {
            Audience audience = db.Audiences.Find(id);
            if (audience == null)
            {
                return NotFound();
            }

            db.Audiences.Remove(audience);
            db.SaveChanges(User.Identity.GetUserName());
            OnCrudOperation(new AudiencesControllerEventArgs(audience, CrudEventEnumStatus.Delete));

            return Ok(audience);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool AudienceExists(string id)
        {
            return db.Audiences.Count(e => e.ClientId == id) > 0;
        }
    }
}