﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Kendo.DynamicLinq;
using Newtonsoft.Json;
using Domain.Models;
using App.DAL;
using App.Enums;
using App.Helper;
using Domain.Models.ViewModels;
using Microsoft.AspNet.Identity;

namespace App.Controllers.WebApi
{
    public class CategoriesControllerEventArgs : EventArgs
    {
        public readonly Category After;
        public readonly CrudEventEnumStatus Status;

        public CategoriesControllerEventArgs(Category after, CrudEventEnumStatus status)
        {
            this.After = after;
            this.Status = status;
        }
    }

    public class CategoriesController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public static event EventHandler<CategoriesControllerEventArgs> AfterCall;

        protected virtual void OnCrudOperation(CategoriesControllerEventArgs e)
        {
            if (AfterCall != null)
            {
                AfterCall(this, e);
            }
        }

        // GET: api/Categories
        public DataSourceResult GetCategories(HttpRequestMessage requestMessage)
        {
            DataSourceRequest request =
                JsonConvert.DeserializeObject<DataSourceRequest>(requestMessage.RequestUri.ParseQueryString().GetValues("q")[0]);

            if (request.Filter != null && !request.Filter.Filters.Any())
            {
                request.Filter = null;
            }
            else
            {
                request.Filter.ToCustomExpression();
            }

            var result = db.Categories.Include(x => x.Products).AsQueryable();

            return result
                .OrderBy(x => x.Id)
                .Project()
                .To<CategoryViewModel>()
                .ToDataSourceResult(request.Take, request.Skip, request.Sort, request.Filter);
        }

        // GET: api/Categories/5
        [ResponseType(typeof(CategoryViewModel))]
        public IHttpActionResult GetCategory(int id)
        {
            Category category = db.Categories.Find(id);
            if (category == null)
            {
                return NotFound();
            }

            return Ok(Mapper.Map<Category, CategoryViewModel>(category));
        }

        // PUT: api/Categories/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutCategory(int id, CategoryViewModel category)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != category.Id)
            {
                return BadRequest();
            }

            var tempcategory = category.ToEntity();

            db.Entry(tempcategory).State = EntityState.Modified;

            try
            {
                db.SaveChanges(User.Identity.GetUserName());
                OnCrudOperation(new CategoriesControllerEventArgs(tempcategory, CrudEventEnumStatus.Update));
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CategoryExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Categories
        [ResponseType(typeof(CategoryViewModel))]
        public IHttpActionResult PostCategory(CategoryViewModel category)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var tempcategory = category.ToEntity();

            db.Categories.Add(tempcategory);
            db.SaveChanges(User.Identity.GetUserName());
            OnCrudOperation(new CategoriesControllerEventArgs(tempcategory, CrudEventEnumStatus.Create));

            return CreatedAtRoute("DefaultApi", new { id = category.Id }, 
                    Mapper.Map<Category, CategoryViewModel>(tempcategory));
        }

        // DELETE: api/Categories/5
        [ResponseType(typeof(CategoryViewModel))]
        public IHttpActionResult DeleteCategory(int id)
        {
            Category category = db.Categories.Find(id);
            if (category == null)
            {
                return NotFound();
            }

            db.Categories.Remove(category);
            db.SaveChanges(User.Identity.GetUserName());
            OnCrudOperation(new CategoriesControllerEventArgs(category, CrudEventEnumStatus.Delete));

            return Ok(Mapper.Map<Category, CategoryViewModel>(category));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CategoryExists(int id)
        {
            return db.Categories.Count(e => e.Id == id) > 0;
        }
    }
}