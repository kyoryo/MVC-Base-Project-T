﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Kendo.DynamicLinq;
using Newtonsoft.Json;
using Domain.Models;
using App.DAL;
using App.Enums;
using App.Helper;
using Domain.Models.ViewModels;
using Microsoft.AspNet.Identity;

namespace App.Controllers.WebApi
{
    public class TicketCommentsControllerEventArgs : EventArgs
    {
        public readonly TicketComment After;
        public readonly CrudEventEnumStatus Status;

        public TicketCommentsControllerEventArgs(TicketComment after, CrudEventEnumStatus status)
        {
            this.After = after;
            this.Status = status;
        }
    }

    public class TicketCommentsController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public static event EventHandler<TicketCommentsControllerEventArgs> AfterCall;

        protected virtual void OnCrudOperation(TicketCommentsControllerEventArgs e)
        {
            if (AfterCall != null)
            {
                AfterCall(this, e);
            }
        }

        // GET: api/TicketComments
        public DataSourceResult GetTicketComments(HttpRequestMessage requestMessage)
        {
            DataSourceRequest request =
                JsonConvert.DeserializeObject<DataSourceRequest>(requestMessage.RequestUri.ParseQueryString().GetValues("q")[0]);

            if (request.Filter != null && !request.Filter.Filters.Any())
            {
                request.Filter = null;
            }
            else
            {
                request.Filter.ToCustomExpression();
            }

            var result = db.TicketComments.Include(x => x.Commenter).Include(x => x.Ticket).AsQueryable();

            return result
                .OrderBy(x => x.Id)
                .Project()
                .To<TicketCommentViewModel>()
                .ToDataSourceResult(request.Take, request.Skip, request.Sort, request.Filter);
        }

        // GET: api/TicketComments/5
        [ResponseType(typeof(TicketCommentViewModel))]
        public IHttpActionResult GetTicketComment(Guid id)
        {
            TicketComment ticketComment = db.TicketComments.Find(id);
            if (ticketComment == null)
            {
                return NotFound();
            }

            return Ok(Mapper.Map<TicketComment, TicketCommentViewModel>(ticketComment));
        }

        // PUT: api/TicketComments/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutTicketComment(Guid id, TicketCommentViewModel ticketComment)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != ticketComment.Id)
            {
                return BadRequest();
            }

            var tempticketComment = ticketComment.ToEntity();

            db.Entry(tempticketComment).State = EntityState.Modified;

            try
            {
                db.SaveChanges(User.Identity.GetUserName());
                OnCrudOperation(new TicketCommentsControllerEventArgs(tempticketComment, CrudEventEnumStatus.Update));
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TicketCommentExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/TicketComments
        [ResponseType(typeof(TicketCommentViewModel))]
        public IHttpActionResult PostTicketComment(TicketCommentViewModel ticketComment)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var tempticketComment = ticketComment.ToEntity();

            db.TicketComments.Add(tempticketComment);
            db.SaveChanges(User.Identity.GetUserName());
            OnCrudOperation(new TicketCommentsControllerEventArgs(tempticketComment, CrudEventEnumStatus.Create));

            return CreatedAtRoute("DefaultApi", new { id = ticketComment.Id }, 
                    Mapper.Map<TicketComment, TicketCommentViewModel>(tempticketComment));
        }

        // DELETE: api/TicketComments/5
        [ResponseType(typeof(TicketCommentViewModel))]
        public IHttpActionResult DeleteTicketComment(Guid id)
        {
            TicketComment ticketComment = db.TicketComments.Find(id);
            if (ticketComment == null)
            {
                return NotFound();
            }

            db.TicketComments.Remove(ticketComment);
            db.SaveChanges(User.Identity.GetUserName());
            OnCrudOperation(new TicketCommentsControllerEventArgs(ticketComment, CrudEventEnumStatus.Delete));

            return Ok(Mapper.Map<TicketComment, TicketCommentViewModel>(ticketComment));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TicketCommentExists(Guid id)
        {
            return db.TicketComments.Count(e => e.Id == id) > 0;
        }
    }
}