﻿using System.Data.Entity;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using App.DAL;
using App.Helper;
using App.Models;
using Kendo.DynamicLinq;
using Newtonsoft.Json;

namespace App.Controllers.WebApi
{
    [RoutePrefix("api/logs")]
    public class LogsController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        [Route("{type}/{id}")]
        public object GetLogs(string type, string id, HttpRequestMessage requestMessage)
        {
            if (requestMessage.RequestUri.ParseQueryString().GetValues("q") == null)
            {
                return BadRequest();
            }

            var request =
                JsonConvert.DeserializeObject<DataSourceRequest>(Helper.Helper.GetQueryStringValue("q", requestMessage));

            request.Filter.ToCustomExpression();

            var temp = db.LogDetails.Where(x => x.Log.TableName == type && x.Log.RecordId == id).AsQueryable();

            var result =
                temp.OrderByDescending(x => x.Log.EventDateUTC)
                    .AsEnumerable()
                    .Select(
                        x =>
                            new Logs()
                            {
                                AuditLogId = x.AuditLogId,
                                RecordId = x.Log.RecordId,
                                EventDateUTC = x.Log.EventDateUTC.DateTime,
                                EventType = x.Log.EventType.ToString(),
                                TableName = x.Log.TableName,
                                UserName = x.Log.UserName,
                                Id = x.Id,
                                ColumnName = x.ColumnName,
                                NewValue = x.NewValue,
                                OriginalValue = x.OriginalValue
                            })
                    .AsQueryable();

            result = KendoSource.Filter(result, request.Filter);

            result = KendoSource.Sort(result, request.Sort);

            var total = result.Count();

            result = result.Skip(request.Skip).Take(request.Take);

            return Json(new {Data = result, Total = total});
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}