﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Kendo.DynamicLinq;
using Newtonsoft.Json;
using Domain.Models;
using App.DAL;
using App.Enums;
using App.Helper;
using Domain.Models.ViewModels;
using Microsoft.AspNet.Identity;

namespace App.Controllers.WebApi
{
    public class SolutionsControllerEventArgs : EventArgs
    {
        public readonly Solution After;
        public readonly CrudEventEnumStatus Status;

        public SolutionsControllerEventArgs(Solution after, CrudEventEnumStatus status)
        {
            this.After = after;
            this.Status = status;
        }
    }

    public class SolutionsController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public static event EventHandler<SolutionsControllerEventArgs> AfterCall;

        protected virtual void OnCrudOperation(SolutionsControllerEventArgs e)
        {
            if (AfterCall != null)
            {
                AfterCall(this, e);
            }
        }

        // GET: api/Solutions
        public DataSourceResult GetSolutions(HttpRequestMessage requestMessage)
        {
            DataSourceRequest request =
                JsonConvert.DeserializeObject<DataSourceRequest>(requestMessage.RequestUri.ParseQueryString().GetValues("q")[0]);

            if (request.Filter != null && !request.Filter.Filters.Any())
            {
                request.Filter = null;
            }
            else
            {
                request.Filter.ToCustomExpression();
            }

            var result = db.Solutions.Include(x => x.Creator).Include(x => x.Tickets).AsQueryable();

            return result
                .OrderBy(x => x.Id)
                .Project()
                .To<SolutionViewModel>()
                .ToDataSourceResult(request.Take, request.Skip, request.Sort, request.Filter);
        }

        // GET: api/Solutions/5
        [ResponseType(typeof(SolutionViewModel))]
        public IHttpActionResult GetSolution(int id)
        {
            Solution solution = db.Solutions.Find(id);
            if (solution == null)
            {
                return NotFound();
            }

            return Ok(Mapper.Map<Solution, SolutionViewModel>(solution));
        }

        // PUT: api/Solutions/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutSolution(int id, SolutionViewModel solution)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != solution.Id)
            {
                return BadRequest();
            }

            var tempsolution = solution.ToEntity();

            db.Entry(tempsolution).State = EntityState.Modified;

            try
            {
                db.SaveChanges(User.Identity.GetUserName());
                OnCrudOperation(new SolutionsControllerEventArgs(tempsolution, CrudEventEnumStatus.Update));
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SolutionExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Solutions
        [ResponseType(typeof(SolutionViewModel))]
        public IHttpActionResult PostSolution(SolutionViewModel solution)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var tempsolution = solution.ToEntity();

            db.Solutions.Add(tempsolution);
            db.SaveChanges(User.Identity.GetUserName());
            OnCrudOperation(new SolutionsControllerEventArgs(tempsolution, CrudEventEnumStatus.Create));

            return CreatedAtRoute("DefaultApi", new { id = solution.Id }, 
                    Mapper.Map<Solution, SolutionViewModel>(tempsolution));
        }

        // DELETE: api/Solutions/5
        [ResponseType(typeof(SolutionViewModel))]
        public IHttpActionResult DeleteSolution(int id)
        {
            Solution solution = db.Solutions.Find(id);
            if (solution == null)
            {
                return NotFound();
            }

            db.Solutions.Remove(solution);
            db.SaveChanges(User.Identity.GetUserName());
            OnCrudOperation(new SolutionsControllerEventArgs(solution, CrudEventEnumStatus.Delete));

            return Ok(Mapper.Map<Solution, SolutionViewModel>(solution));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool SolutionExists(int id)
        {
            return db.Solutions.Count(e => e.Id == id) > 0;
        }
    }
}