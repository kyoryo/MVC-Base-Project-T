﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Kendo.DynamicLinq;
using Newtonsoft.Json;
using Domain.Infrastructure;
using App.DAL;
using App.Enums;
using App.Helper;
using Domain.Models.ViewModels;
using Microsoft.AspNet.Identity;

namespace App.Controllers.WebApi
{
    public class MenusControllerEventArgs : EventArgs
    {
        public readonly Menu After;
        public readonly CrudEventEnumStatus Status;

        public MenusControllerEventArgs(Menu after, CrudEventEnumStatus status)
        {
            this.After = after;
            this.Status = status;
        }
    }

    public class MenusController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public static event EventHandler<MenusControllerEventArgs> AfterCall;

        protected virtual void OnCrudOperation(MenusControllerEventArgs e)
        {
            if (AfterCall != null)
            {
                AfterCall(this, e);
            }
        }

        // GET: api/Menus
        public DataSourceResult GetMenus(HttpRequestMessage requestMessage)
        {
            DataSourceRequest request =
                JsonConvert.DeserializeObject<DataSourceRequest>(requestMessage.RequestUri.ParseQueryString().GetValues("q")[0]);

            if (request.Filter != null && !request.Filter.Filters.Any())
            {
                request.Filter = null;
            }
            else
            {
                request.Filter.ToCustomExpression();
            }

            var result = db.Menus.Include(x => x.RoleMenus).AsQueryable();

            if (!string.IsNullOrWhiteSpace(Helper.Helper.GetQueryStringValue("role", requestMessage)))
            {
                var roleId = Helper.Helper.GetQueryStringValue("role", requestMessage);
                var registeredRoles = db.RoleMenus.Where(x => x.RoleId.ToString() == roleId);

                result = result.Where(x => !registeredRoles.Any(r => r.MenuId == x.Id));
            }

            return result
                .OrderBy(x => x.Name)
                .Project()
                .To<MenuViewModel>()
                .ToDataSourceResult(request.Take, request.Skip, request.Sort, request.Filter);
        }

        // GET: api/Menus/5
        [ResponseType(typeof(MenuViewModel))]
        public IHttpActionResult GetMenu(Guid id)
        {
            Menu menu = db.Menus.Find(id);
            if (menu == null)
            {
                return NotFound();
            }

            return Ok(Mapper.Map<Menu, MenuViewModel>(menu));
        }

        // PUT: api/Menus/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutMenu(Guid id, MenuViewModel menu)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != menu.Id)
            {
                return BadRequest();
            }

            var tempmenu = menu.ToEntity();

            db.Entry(tempmenu).State = EntityState.Modified;

            try
            {
                db.SaveChanges(User.Identity.GetUserName());
                OnCrudOperation(new MenusControllerEventArgs(tempmenu, CrudEventEnumStatus.Update));
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MenuExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Menus
        [ResponseType(typeof(MenuViewModel))]
        public IHttpActionResult PostMenu(MenuViewModel menu)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var tempmenu = menu.ToEntity();

            db.Menus.Add(tempmenu);
            db.SaveChanges(User.Identity.GetUserName());
            OnCrudOperation(new MenusControllerEventArgs(tempmenu, CrudEventEnumStatus.Create));

            return CreatedAtRoute("DefaultApi", new { id = menu.Id }, 
                    Mapper.Map<Menu, MenuViewModel>(tempmenu));
        }

        // DELETE: api/Menus/5
        [ResponseType(typeof(MenuViewModel))]
        public IHttpActionResult DeleteMenu(Guid id)
        {
            Menu menu = db.Menus.Find(id);
            if (menu == null)
            {
                return NotFound();
            }

            db.Menus.Remove(menu);
            db.SaveChanges(User.Identity.GetUserName());
            OnCrudOperation(new MenusControllerEventArgs(menu, CrudEventEnumStatus.Delete));

            return Ok(Mapper.Map<Menu, MenuViewModel>(menu));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool MenuExists(Guid id)
        {
            return db.Menus.Count(e => e.Id == id) > 0;
        }
    }
}