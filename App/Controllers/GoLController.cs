﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using App.Helper;
using System.Web.UI;

namespace App.Controllers
{
    public class GoLController : Controller
    {
        // GET: GoL
 
        public ActionResult Index()
        {
            GoLHelpers.InitializeGame();
            GoLHelpers.AddGlider(GoLHelpers.arrOne, 10, 10);
            GoLHelpers.DrawStartIteration();
            HtmlString table = new HtmlString(GoLHelpers.table.ToString());
            return View(table);
        }

        public ActionResult Theory()
        {
            return View();
        }

        [OutputCache(NoStore=true, Location= OutputCacheLocation.Client, Duration=1)]
        public ActionResult Update()
        {
            return PartialView("_Table", NextIteration());
        }

        public ActionResult Reset()
        {
            return PartialView("_Table", CleanTable());
        }
        public ActionResult AddGlider()
        {
            return PartialView("_Table", AddGliderToTable());
        }

        public HtmlString AddGliderToTable(){
            GoLHelpers.AddGlider(GoLHelpers.arrOne, 10, 10);
            GoLHelpers.DrawNextIteration();
            GoLHelpers.DrawNextIteration();
            GoLHelpers.DrawNextIteration();
            GoLHelpers.DrawNextIteration();

            return new HtmlString(GoLHelpers.table.ToString());
        }

        public HtmlString CleanTable()
        {
            GoLHelpers.InitializeGame();
            GoLHelpers.NewGameTable();
            return new HtmlString(GoLHelpers.table.ToString());
        }

        public HtmlString NextIteration()
        {
            GoLHelpers.DrawNextIteration();
            return new HtmlString(GoLHelpers.table.ToString());
        }

    }
}