﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Domain.Infrastructure;
using App.DAL;
using App.Enums;
using App.Infrastructure;
using Microsoft.AspNet.Identity;

namespace App.Controllers
{
    [PermissionAuthorization("administrator_only")]
    public class RoleMenuEventArgs : EventArgs
    {
        public readonly RoleMenu After;
        public readonly CrudEventEnumStatus Status;

        public RoleMenuEventArgs(RoleMenu after, CrudEventEnumStatus status)
        {
            this.After = after;
            this.Status = status;
        }
    }

    public class RoleMenuController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public static event EventHandler<RoleMenuEventArgs> AfterCall;

        protected virtual void OnCrudOperation(RoleMenuEventArgs e)
        {
            if (AfterCall != null)
            {
                AfterCall(this, e);
            }
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RoleMenu roleMenu = db.RoleMenus.Find(id);
            ViewBag.Id = id;
            if (roleMenu == null)
            {
                return HttpNotFound();
            }
            return View(roleMenu);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(RoleMenu roleMenu)
        {
            if (ModelState.IsValid)
            {
                roleMenu.Id = Guid.NewGuid();
                db.RoleMenus.Add(roleMenu);

                db.SaveChanges(User.Identity.GetUserName());

                OnCrudOperation(new RoleMenuEventArgs(roleMenu, CrudEventEnumStatus.Create));

                if (!string.IsNullOrWhiteSpace(Request.QueryString["createNext"]))
                {
                    if (Request.QueryString["createNext"] == "true")
                    {
                        TempData["Created"] = true;
                        TempData["Alert"] = "Berhasil menambahkan RoleMenu";
                        TempData["AlertClass"] = "success";

                        return RedirectToAction("Create");
                    }
                }

                return RedirectToAction("Details", new { id = roleMenu.Id});
            }

            TempData["Alert"] = "Gagal menambahkan RoleMenu. Harap periksa kembali data yang anda input.";
            TempData["AlertClass"] = "danger";

            return View(roleMenu);
        }

        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RoleMenu roleMenu = db.RoleMenus.Find(id);
            ViewBag.Id = id;

            if (roleMenu == null)
            {
                return HttpNotFound();
            }

            return View(roleMenu);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(RoleMenu roleMenu)
        {
            if (ModelState.IsValid)
            {
                db.Entry(roleMenu).State = EntityState.Modified;

                db.SaveChanges(User.Identity.GetUserName());

                OnCrudOperation(new RoleMenuEventArgs(roleMenu, CrudEventEnumStatus.Update));

                TempData["Alert"] = "Berhasil menyimpan perubahan RoleMenu";
                TempData["AlertClass"] = "success";

                return RedirectToAction("Details", new { id = roleMenu.Id});
            }
            ViewBag.Id = roleMenu.Id;

            TempData["Alert"] = "Gagal melakukan perubahan RoleMenu. Harap periksa kembali data yang anda masukkan.";
            TempData["AlertClass"] = "danger";

            return View(roleMenu);
        }

        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            RoleMenu roleMenu = db.RoleMenus.Find(id);
            ViewBag.Id = id;

            if (roleMenu == null)
            {
                return HttpNotFound();
            }
            return View(roleMenu);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            RoleMenu roleMenu = db.RoleMenus.Find(id);
            ViewBag.Id = id;
            db.RoleMenus.Remove(roleMenu);
            db.SaveChanges(User.Identity.GetUserName());
            OnCrudOperation(new RoleMenuEventArgs(roleMenu, CrudEventEnumStatus.Delete));
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
