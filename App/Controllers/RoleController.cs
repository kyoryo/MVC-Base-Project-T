﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using App.Infrastructure;
using Domain.Infrastructure;
using Domain.Models;
using Microsoft.AspNet.Identity;

namespace App.Controllers
{
    [PermissionAuthorization("administrator_only")]
    public class RoleController : BaseUserController
    {
        // GET: Role
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var role = AppRoleManager.FindById(id);

            if (role == null)
            {
                return HttpNotFound();
            }
            return View(role);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ApplicationRole model)
        {
            if (ModelState.IsValid)
            {
                var result = AppRoleManager.Create(model);

                if (result.Succeeded)
                {
                    if (!string.IsNullOrWhiteSpace(Request.QueryString["createNext"]))
                    {
                        if (Request.QueryString["createNext"] == "true")
                        {
                            TempData["Created"] = true;
                            TempData["Alert"] = "Berhasil menambahkan Role";
                            TempData["AlertClass"] = "success";

                            return RedirectToAction("Create");
                        }
                    }

                    return RedirectToAction("Details", new { id = model.Id });
                }
            }

            TempData["Alert"] = "Gagal menambahkan Role. Harap periksa kembali data yang anda input.";
            TempData["AlertClass"] = "danger";

            return View(model);
        }

        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var role = AppRoleManager.FindById(id);

            if (role == null)
            {
                return HttpNotFound();
            }
            return View(role);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ApplicationRole model)
        {
            if (ModelState.IsValid)
            {
                var role = AppRoleManager.FindById(model.Id);
                role.Name = model.Name;
                role.Description = model.Description;
                var result = AppRoleManager.Update(role);

                if (result.Succeeded)
                {
                    TempData["Alert"] = "Berhasil menyimpan perubahan Role";
                    TempData["AlertClass"] = "success";

                    return RedirectToAction("Details", new { id = model.Id });
                }
            }
            ViewBag.Id = model.Id;

            TempData["Alert"] = "Gagal melakukan perubahan Role. Harap periksa kembali data yang anda masukkan.";
            TempData["AlertClass"] = "danger";

            return View(model);
        }
    }
}