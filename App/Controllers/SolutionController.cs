﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Domain.Models;
using App.DAL;
using App.Enums;
using Microsoft.AspNet.Identity;

namespace App.Controllers
{
    public class SolutionEventArgs : EventArgs
    {
        public readonly Solution After;
        public readonly CrudEventEnumStatus Status;

        public SolutionEventArgs(Solution after, CrudEventEnumStatus status)
        {
            this.After = after;
            this.Status = status;
        }
    }

    public class SolutionController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public static event EventHandler<SolutionEventArgs> AfterCall;

        protected virtual void OnCrudOperation(SolutionEventArgs e)
        {
            if (AfterCall != null)
            {
                AfterCall(this, e);
            }
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Solution solution = db.Solutions.Find(id);
            ViewBag.Id = id;
            if (solution == null)
            {
                return HttpNotFound();
            }
            return View(solution);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Solution solution)
        {
            if (ModelState.IsValid)
            {
                solution.CreatorId = User.Identity.GetUserId();

                db.Solutions.Add(solution);

                db.SaveChanges(User.Identity.GetUserName());

                OnCrudOperation(new SolutionEventArgs(solution, CrudEventEnumStatus.Create));

                if (!string.IsNullOrWhiteSpace(Request.QueryString["createNext"]))
                {
                    if (Request.QueryString["createNext"] == "true")
                    {
                        TempData["Created"] = true;
                        TempData["Alert"] = "Berhasil menambahkan Solution";
                        TempData["AlertClass"] = "success";

                        return RedirectToAction("Create");
                    }
                }

                return RedirectToAction("Details", new { id = solution.Id});
            }

            TempData["Alert"] = "Gagal menambahkan Solution. Harap periksa kembali data yang anda input.";
            TempData["AlertClass"] = "danger";

            return View(solution);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Solution solution = db.Solutions.Find(id);
            ViewBag.Id = id;

            if (solution == null)
            {
                return HttpNotFound();
            }

            return View(solution);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Solution solution)
        {
            if (ModelState.IsValid)
            {
                db.Entry(solution).State = EntityState.Modified;

                db.SaveChanges(User.Identity.GetUserName());

                OnCrudOperation(new SolutionEventArgs(solution, CrudEventEnumStatus.Update));

                TempData["Alert"] = "Berhasil menyimpan perubahan Solution";
                TempData["AlertClass"] = "success";

                return RedirectToAction("Details", new { id = solution.Id});
            }
            ViewBag.Id = solution.Id;

            TempData["Alert"] = "Gagal melakukan perubahan Solution. Harap periksa kembali data yang anda masukkan.";
            TempData["AlertClass"] = "danger";

            return View(solution);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Solution solution = db.Solutions.Find(id);
            ViewBag.Id = id;

            if (solution == null)
            {
                return HttpNotFound();
            }
            return View(solution);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Solution solution = db.Solutions.Find(id);
            ViewBag.Id = id;
            db.Solutions.Remove(solution);
            db.SaveChanges(User.Identity.GetUserName());
            OnCrudOperation(new SolutionEventArgs(solution, CrudEventEnumStatus.Delete));
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
