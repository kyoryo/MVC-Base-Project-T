﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Domain.Infrastructure;
using App.DAL;
using App.Enums;
using Microsoft.AspNet.Identity;

namespace App.Controllers
{
    public class PermissionEventArgs : EventArgs
    {
        public readonly Permission After;
        public readonly CrudEventEnumStatus Status;

        public PermissionEventArgs(Permission after, CrudEventEnumStatus status)
        {
            this.After = after;
            this.Status = status;
        }
    }

    public class PermissionController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public static event EventHandler<PermissionEventArgs> AfterCall;

        protected virtual void OnCrudOperation(PermissionEventArgs e)
        {
            if (AfterCall != null)
            {
                AfterCall(this, e);
            }
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Permission permission = db.Permissions.Find(id);
            ViewBag.Id = id;
            if (permission == null)
            {
                return HttpNotFound();
            }
            return View(permission);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Permission permission)
        {
            if (ModelState.IsValid)
            {
                permission.Id = Guid.NewGuid();
                db.Permissions.Add(permission);

                db.SaveChanges(User.Identity.GetUserName());

                OnCrudOperation(new PermissionEventArgs(permission, CrudEventEnumStatus.Create));

                if (!string.IsNullOrWhiteSpace(Request.QueryString["createNext"]))
                {
                    if (Request.QueryString["createNext"] == "true")
                    {
                        TempData["Created"] = true;
                        TempData["Alert"] = "Berhasil menambahkan Permission";
                        TempData["AlertClass"] = "success";

                        return RedirectToAction("Create");
                    }
                }

                return RedirectToAction("Details", new { id = permission.Id});
            }

            TempData["Alert"] = "Gagal menambahkan Permission. Harap periksa kembali data yang anda input.";
            TempData["AlertClass"] = "danger";

            return View(permission);
        }

        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Permission permission = db.Permissions.Find(id);
            ViewBag.Id = id;

            if (permission == null)
            {
                return HttpNotFound();
            }

            return View(permission);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Permission permission)
        {
            if (ModelState.IsValid)
            {
                db.Entry(permission).State = EntityState.Modified;

                db.SaveChanges(User.Identity.GetUserName());

                OnCrudOperation(new PermissionEventArgs(permission, CrudEventEnumStatus.Update));

                TempData["Alert"] = "Berhasil menyimpan perubahan Permission";
                TempData["AlertClass"] = "success";

                return RedirectToAction("Details", new { id = permission.Id});
            }
            ViewBag.Id = permission.Id;

            TempData["Alert"] = "Gagal melakukan perubahan Permission. Harap periksa kembali data yang anda masukkan.";
            TempData["AlertClass"] = "danger";

            return View(permission);
        }

        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Permission permission = db.Permissions.Find(id);
            ViewBag.Id = id;

            if (permission == null)
            {
                return HttpNotFound();
            }
            return View(permission);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            Permission permission = db.Permissions.Find(id);
            ViewBag.Id = id;
            db.Permissions.Remove(permission);
            db.SaveChanges(User.Identity.GetUserName());
            OnCrudOperation(new PermissionEventArgs(permission, CrudEventEnumStatus.Delete));
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
