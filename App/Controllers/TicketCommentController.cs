﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Domain.Models;
using App.DAL;
using App.Enums;
using Microsoft.AspNet.Identity;

namespace App.Controllers
{
    public class TicketCommentEventArgs : EventArgs
    {
        public readonly TicketComment After;
        public readonly CrudEventEnumStatus Status;

        public TicketCommentEventArgs(TicketComment after, CrudEventEnumStatus status)
        {
            this.After = after;
            this.Status = status;
        }
    }

    public class TicketCommentController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public static event EventHandler<TicketCommentEventArgs> AfterCall;

        protected virtual void OnCrudOperation(TicketCommentEventArgs e)
        {
            if (AfterCall != null)
            {
                AfterCall(this, e);
            }
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TicketComment ticketComment = db.TicketComments.Find(id);
            ViewBag.Id = id;
            if (ticketComment == null)
            {
                return HttpNotFound();
            }
            return View(ticketComment);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(TicketComment ticketComment)
        {
            if (ModelState.IsValid)
            {
                ticketComment.Id = Guid.NewGuid();
                ticketComment.CommenterId = User.Identity.GetUserId();
                db.TicketComments.Add(ticketComment);

                db.SaveChanges(User.Identity.GetUserName());

                OnCrudOperation(new TicketCommentEventArgs(ticketComment, CrudEventEnumStatus.Create));

                if (!string.IsNullOrWhiteSpace(Request.QueryString["createNext"]))
                {
                    if (Request.QueryString["createNext"] == "true")
                    {
                        TempData["Created"] = true;
                        TempData["Alert"] = "Berhasil menambahkan TicketComment";
                        TempData["AlertClass"] = "success";

                        return RedirectToAction("Create");
                    }
                }

                return RedirectToAction("Details", new { id = ticketComment.Id});
            }

            TempData["Alert"] = "Gagal menambahkan TicketComment. Harap periksa kembali data yang anda input.";
            TempData["AlertClass"] = "danger";

            return View(ticketComment);
        }

        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TicketComment ticketComment = db.TicketComments.Find(id);
            ViewBag.Id = id;
            //ticketComment.CommenterId = ticketComment.CommenterId.Where(c =>)

            if (ticketComment == null)
            {
                return HttpNotFound();
            }

            return View(ticketComment);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(TicketComment ticketComment)
        {
            if (ModelState.IsValid)
            {
                db.Entry(ticketComment).State = EntityState.Modified;

                db.SaveChanges(User.Identity.GetUserName());

                OnCrudOperation(new TicketCommentEventArgs(ticketComment, CrudEventEnumStatus.Update));

                TempData["Alert"] = "Berhasil menyimpan perubahan TicketComment";
                TempData["AlertClass"] = "success";

                return RedirectToAction("Details", new { id = ticketComment.Id});
            }
            ViewBag.Id = ticketComment.Id;

            TempData["Alert"] = "Gagal melakukan perubahan TicketComment. Harap periksa kembali data yang anda masukkan.";
            TempData["AlertClass"] = "danger";

            return View(ticketComment);
        }

        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            TicketComment ticketComment = db.TicketComments.Find(id);
            ViewBag.Id = id;

            if (ticketComment == null)
            {
                return HttpNotFound();
            }
            return View(ticketComment);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            TicketComment ticketComment = db.TicketComments.Find(id);
            ViewBag.Id = id;
            db.TicketComments.Remove(ticketComment);
            db.SaveChanges(User.Identity.GetUserName());
            OnCrudOperation(new TicketCommentEventArgs(ticketComment, CrudEventEnumStatus.Delete));
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
