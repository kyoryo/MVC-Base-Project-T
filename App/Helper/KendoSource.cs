﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using Kendo.DynamicLinq;

namespace App.Helper
{
    public static class KendoSource
    {
        public static IQueryable<T> Filter<T>(IQueryable<T> queryable, Filter filter)
        {
            if (filter != null && filter.Logic != null)
            {
                var filters = filter.All();
                var objArray =
                    Enumerable.ToArray<object>(Enumerable.Select<Filter, object>((IEnumerable<Filter>) filters,
                        (Func<Filter, object>) (f => f.Value)));
                var str = filter.ToExpression(filters);
                queryable = DynamicQueryable.Where<T>(queryable, str, objArray);
            }
            return queryable;
        }

        public static IQueryable<T> Sort<T>(IQueryable<T> queryable, IEnumerable<Sort> sort)
        {
            if (sort == null || !Enumerable.Any<Sort>(sort))
            {
                return queryable;
            }
            var str = string.Join(",",
                Enumerable.Select<Sort, string>(sort, (Func<Sort, string>) (s => s.ToExpression())));
            return DynamicQueryable.OrderBy<T>(queryable, str, new object[0]);
        }

        public static void ToCustomExpression(this Filter filter)
        {
            if (filter != null)
            {
                if (filter.Filters.Any(x => x.Value.ToString().Contains("guid")))
                {
                    foreach (var item in filter.Filters.Where(x => x.Value.ToString().Contains("guid")))
                    {
                        if (item.Value.ToString().Contains("guid"))
                        {
                            item.Field += ".ToString()";
                            item.Value = item.Value.ToString().Replace("guid'", "").TrimEnd('\'');
                        }
                    }
                }

                var toLowerOperator = new string[]
                {
                    "contains",
                    "doesnotcontain",
                    "startswith",
                    "endswith"
                };

                if (filter.Filters.Any(x => toLowerOperator.Contains(x.Operator)))
                {
                    foreach (var item in filter.Filters.Where(x => toLowerOperator.Contains(x.Operator)))
                    {
                        if (toLowerOperator.Contains(item.Operator))
                        {
                            item.Field += ".ToLower()";
                            item.Value = item.Value.ToString().ToLower();
                        }
                    }
                }
            }
        }
    }
}