﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IdentityModel.Tokens;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using App.DAL;
using Domain.Infrastructure;
using Newtonsoft.Json.Linq;

namespace App.Helper
{
    public static class UserHelper
    {
        public static ApplicationUser GetUser(string userId)
        {
            using (var db = new ApplicationDbContext())
            {
                var user = db.Users.Include(x => x.UserProfile).FirstOrDefault(x => x.Id == userId);

                return user;
            }
        }

        public static string GetUserPhoto(string userId)
        {
            using (var db = new ApplicationDbContext())
            {
                var user = db.Users.Find(userId);
                var url = user.UserProfile.Photo;

                if (!string.IsNullOrWhiteSpace(url))
                {
                    var temp = HttpContext.Current.Server.MapPath(url);
                    if (File.Exists(temp))
                    {
                        return url;
                    }
                }

                return "/Content/images/no_image_available.png";
            }
        }
    }
}