namespace App.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class second : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Solution",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false),
                        Content = c.String(nullable: false),
                        CreatorId = c.String(maxLength: 128),
                        CreatedAt = c.DateTime(nullable: false),
                        LastUpdateTime = c.DateTime(),
                        IsPublished = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.CreatorId)
                .Index(t => t.CreatorId);
            
            CreateTable(
                "dbo.Ticket",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false),
                        Description = c.String(),
                        Category = c.Int(nullable: false),
                        Status = c.Int(nullable: false),
                        Application = c.Int(nullable: false),
                        Priority = c.Int(nullable: false),
                        DueDate = c.DateTime(),
                        CreatorId = c.String(maxLength: 128),
                        SolutionId = c.Int(),
                        CreatedAt = c.DateTime(nullable: false),
                        LastUpdateTime = c.DateTime(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.CreatorId)
                .ForeignKey("dbo.Solution", t => t.SolutionId)
                .Index(t => t.CreatorId)
                .Index(t => t.SolutionId);
            
            CreateTable(
                "dbo.TicketComment",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        TicketId = c.Int(nullable: false),
                        Content = c.String(nullable: false),
                        CommenterId = c.String(maxLength: 128),
                        CreatedAt = c.DateTime(nullable: false),
                        LastUpdateTime = c.DateTime(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.CommenterId)
                .ForeignKey("dbo.Ticket", t => t.TicketId, cascadeDelete: true)
                .Index(t => t.TicketId)
                .Index(t => t.CommenterId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Ticket", "SolutionId", "dbo.Solution");
            DropForeignKey("dbo.Ticket", "CreatorId", "dbo.AspNetUsers");
            DropForeignKey("dbo.TicketComment", "TicketId", "dbo.Ticket");
            DropForeignKey("dbo.TicketComment", "CommenterId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Solution", "CreatorId", "dbo.AspNetUsers");
            DropIndex("dbo.TicketComment", new[] { "CommenterId" });
            DropIndex("dbo.TicketComment", new[] { "TicketId" });
            DropIndex("dbo.Ticket", new[] { "SolutionId" });
            DropIndex("dbo.Ticket", new[] { "CreatorId" });
            DropIndex("dbo.Solution", new[] { "CreatorId" });
            DropTable("dbo.TicketComment");
            DropTable("dbo.Ticket");
            DropTable("dbo.Solution");
        }
    }
}
