﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using App.Services;
using Microsoft.AspNet.Identity;

namespace App.Infrastructure
{
    public class HandleMenuAttribute : ActionFilterAttribute
    {
        private readonly IAccountService _accountService;

        public HandleMenuAttribute()
        {
            _accountService = new AccountService();
        }

        public HandleMenuAttribute(IAccountService accountService)
        {
            _accountService = accountService;
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var userId = filterContext.HttpContext.User.Identity.GetUserId();

            //filterContext.Result = new HttpNotFoundResult();

            if (!_accountService.HasMenu(userId, filterContext.ActionDescriptor.ActionName, filterContext.ActionDescriptor.ControllerDescriptor.ControllerName))
                filterContext.Result = new HttpStatusCodeResult((int)System.Net.HttpStatusCode.Forbidden);

            base.OnActionExecuting(filterContext);
        }
    }
}