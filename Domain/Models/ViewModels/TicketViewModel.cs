﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using AutoMapper;
using Heroic.AutoMapper;
using Domain.Infrastructure;
using Domain.Enums;

namespace Domain.Models.ViewModels
{
    public class TicketViewModel : IMapFrom<Ticket>, IHaveCustomMappings
    {
        public ApplicationUser Creator { get; set; }
        public Solution Solution { get; set; }
        public int Id { get; set; }
        [Required]
        public string Title { get; set; }
        public string Description { get; set; }
        public TicketCategory Category { get; set; }
        public TicketStatus Status { get; set; }
        public TicketApplication Application { get; set; }
        public TicketPriority Priority { get; set; }
        public DateTime? DueDate { get; set; }
        public string CreatorId { get; set; }
        public int? SolutionId { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? LastUpdateTime { get; set; }
        
        public void CreateMappings(IConfiguration configuration)
        {
            configuration.CreateMap<Ticket, TicketViewModel>();
        }

        public virtual Ticket ToEntity()
        {
            return new Ticket()
            {
                Id = Id,
                Title = Title,
                Description = Description,
                Category = Category,
                Status = Status,
                Application = Application,
                Priority = Priority,
                DueDate = DueDate,
                CreatorId = CreatorId,
                SolutionId = SolutionId,
                CreatedAt = CreatedAt,
                LastUpdateTime = LastUpdateTime,
            };
        }
    }
}

