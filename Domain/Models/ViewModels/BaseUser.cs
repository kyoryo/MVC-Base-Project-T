﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using AutoMapper;
using Domain.Infrastructure;
using Domain.Models.Interface;
using Heroic.AutoMapper;
using Humanizer;
using Scaffold;

namespace Domain.Models.ViewModels
{
    public class BaseUser : IBaseUser, IMapFrom<ApplicationUser>, IHaveCustomMappings
    {
        public string Id { get; set; }

        public int UserProfileId { get; set; }

        [Required]
        [DisplayName("Username")]
        public string UserName { get; set; }

        [Required]
        [DisplayName("Email")]
        public string Email { get; set; }

        [Required]
        [DisplayName("Nama")]
        public string Name { get; set; }

        [Required]
        [DisplayName("Nomor Telepon")]
        public string Telephone { get; set; }

        [Required]
        [DisplayName("Alamat")]
        public string Address { get; set; }

        [Required]
        [DisplayName("Tempat Lahir")]
        public string BirthPlace { get; set; }

        [Required]
        [DisplayName("Tanggal Lahir")]
        public DateTime? BirthDate { get; set; }

        [DisplayName("Foto")]
        public string Photo { get; set; }

        [DisplayName("Status")]
        public string Status { get; set; }

        public void CreateMappings(IConfiguration configuration)
        {
            var active = UserStatus.Active.Humanize();
            var notActive = UserStatus.NotActive.Humanize();

            configuration.CreateMap<ApplicationUser, BaseUser>()
                .ForMember(x => x.Name, opt => opt.MapFrom(x => x.UserProfile.Name))
                .ForMember(x => x.Telephone, opt => opt.MapFrom(x => x.PhoneNumber))
                .ForMember(x => x.Address, opt => opt.MapFrom(x => x.UserProfile.Address))
                .ForMember(x => x.BirthPlace, opt => opt.MapFrom(x => x.UserProfile.Birthplace))
                .ForMember(x => x.BirthDate, opt => opt.MapFrom(x => x.UserProfile.Birthdate))
                .ForMember(x => x.Photo, opt => opt.MapFrom(x => x.UserProfile.Photo))
                .ForMember(x => x.Status, opt => opt.MapFrom(x => x.EmailConfirmed ? active : notActive));
        }

        public BaseUser()
        {
        }

        public BaseUser(ApplicationUser user)
        {
            Id = user.Id;
            UserProfileId = user.UserProfile.Id;
            Address = user.UserProfile.Address;
            BirthPlace = user.UserProfile.Birthplace;
            BirthDate = user.UserProfile.Birthdate;
            Telephone = user.PhoneNumber;
            Email = user.Email;
            Name = user.UserProfile.Name;
            UserName = user.UserName;
            Photo = user.UserProfile.Photo;
        }
    }

    public enum UserStatus
    {
        Active,
        NotActive
    }
}
