﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using AutoMapper;
using Domain.Infrastructure;
using Heroic.AutoMapper;

namespace Domain.Models.ViewModels
{
    public class RoleMenuViewModel : IMapFrom<RoleMenu>, IHaveCustomMappings
    {
        public Menu Menu { get; set; }
        public ApplicationRole Role { get; set; }
        public Guid Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? LastUpdateTime { get; set; }
        [Required]
        public string RoleId { get; set; }
        [Required]
        public Guid MenuId { get; set; }
        
        public void CreateMappings(IConfiguration configuration)
        {
            configuration.CreateMap<RoleMenu, RoleMenuViewModel>();
        }

        public virtual RoleMenu ToEntity()
        {
            return new RoleMenu()
            {
                Id = Id,
                CreatedAt = CreatedAt,
                LastUpdateTime = LastUpdateTime,
                RoleId = RoleId,
                MenuId = MenuId,
            };
        }
    }
}

