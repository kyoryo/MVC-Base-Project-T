﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using AutoMapper;
using Heroic.AutoMapper;
using Domain.Infrastructure;

namespace Domain.Models.ViewModels
{
    public class TicketCommentViewModel : IMapFrom<TicketComment>, IHaveCustomMappings
    {
        public ApplicationUser Commenter { get; set; }
        public Ticket Ticket { get; set; }
        public Guid Id { get; set; }
        [Required]
        public int TicketId { get; set; }
        [Required]
        public string Content { get; set; }
        public string CommenterId { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? LastUpdateTime { get; set; }
        
        public void CreateMappings(IConfiguration configuration)
        {
            configuration.CreateMap<TicketComment, TicketCommentViewModel>();
        }

        public virtual TicketComment ToEntity()
        {
            return new TicketComment()
            {
                Id = Id,
                TicketId = TicketId,
                Content = Content,
                CommenterId = CommenterId,
                CreatedAt = CreatedAt,
                LastUpdateTime = LastUpdateTime,
            };
        }
    }
}

