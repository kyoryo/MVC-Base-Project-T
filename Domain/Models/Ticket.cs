﻿using Domain.Enums;
using Domain.Infrastructure;
using Scaffold;
using Scaffold.Models.Interface;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace Domain.Models
{
    [TrackChanges()]
    public class Ticket : IModel<int>, ISoftDeletable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [DisplayName("Subject")]
        public string Title { get; set; }

        [UIHint("RichTextBox")]
        public string Description { get; set; }

        public TicketCategory Category { get; set; }
        public TicketStatus Status { get; set; }
        public TicketApplication Application { get; set; }
        public TicketPriority Priority { get; set; }

        [DisplayName("Due Date")]
        public DateTime? DueDate { get; set; }

        public string CreatorId { get; set; }

        public int? SolutionId { get; set; }

        [ForeignKey("CreatorId")]
        public virtual ApplicationUser Creator { get; set; }

        [ForeignKey("SolutionId")]
        public virtual Solution Solution { get; set; }

        public virtual IList<TicketComment> Comments { get; set; }

        [SkipTracking()]
        [HiddenInput]
        [ToTS(TSFlag.SkipCRUD)]
        public DateTime CreatedAt { get; set; }

        public DateTime? LastUpdateTime { get; set; }

        public bool IsDeleted { get; set; }

        public Ticket()
        {
            this.CreatedAt = DateTime.UtcNow;
        }
    }
}
