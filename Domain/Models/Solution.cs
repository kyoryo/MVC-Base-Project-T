﻿using Domain.Infrastructure;
using Scaffold.Models.Interface;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Models
{
    [TrackChanges()]
    public class Solution : IModel<int>, ISoftDeletable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        public string Title { get; set; }

        [Required]
        public string Content { get; set; }

        public string CreatorId { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime? LastUpdateTime { get; set; }

        public bool IsDeleted { get; set; }

        [DisplayName("Status")]
        public bool IsPublished { get; set; }

        [ForeignKey("CreatorId")]
        public virtual ApplicationUser Creator { get; set; }

        public virtual IList<Ticket> Tickets { get; set; }
    }
}
