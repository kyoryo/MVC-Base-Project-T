﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Enums
{
    public enum TicketCategory
    {
        [Display(Name = "Configuration")]
        [Description("Configuration")]
        Configuration,
        [Display(Name = "System")]
        [Description("System")]
        System,
        [Display(Name = "Speed Connection")]
        [Description("Speed Connection")]
        SpeedConnection,
        [Display(Name = "User Setting")]
        [Description("User Setting")]
        UserSetting,
        [Display(Name = "Database Setting")]
        [Description("Database Setting")]
        DatabaseSetting,
        [Display(Name = "User Interface")]
        [Description("User Interface")]
        UserInterface,
        [Display(Name = "Others")]
        [Description("Others")]
        Others
    }

    public enum TicketApplication
    {
        [Display(Name = "E-Acceptance")]
        [Description("E-Acceptance")]
        EAcceptance,
        [Display(Name = "Geotagging")]
        [Description("Geotagging")]
        Geotagging,
        [Display(Name = "VPT")]
        [Description("VPT")]
        VPT,
        [Display(Name = "Helpdesk")]
        [Description("Helpdesk")]
        Helpdesk
    }

    public enum TicketStatus
    {
        [Description("Waiting Tech")]
        [Display(Name = "Waiting Tech")]
        WaitingTech,
        [Display(Name = "Waiting Customer")]
        [Description("Waiting Customer")]
        WaitingCustomer,
        [Display(Name = "Waiting Approval")]
        [Description("Waiting Approval")]
        WaitingApproval,
        [Display(Name = "Solved")]
        [Description("Solved")]
        Solved
    }

    public enum TicketPriority
    {
        Low,
        Mid,
        High
    }
}
