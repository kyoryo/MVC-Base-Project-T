﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;
using Microsoft.AspNet.Identity.EntityFramework;
using Scaffold;
using Scaffold.Models.Interface;

namespace Domain.Infrastructure
{
    [TrackChanges()]
    public class RoleMenu : IModel<Guid>
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        // Skip this field from audit trail
        [SkipTracking()]
        [Required]
        [ToTS(TSFlag.SkipCRUD)]
        public DateTime CreatedAt { get; set; }

        // Skip this field from audit trail
        [SkipTracking()]
        [HiddenInput]
        [ToTS(TSFlag.SkipCRUD)]
        public DateTime? LastUpdateTime { get; set; }

        public int SortOrder { get; set; }

        #region Relationship
        
        [Required]
        [DisplayName("Role")]
        [Index("IX_Role_Menu_Unique", 0, IsUnique = true)]
        public string RoleId { get; set; }

        [ForeignKey("RoleId")]
        [ToTS(TSFlag.Ignore)]
        public virtual ApplicationRole Role { get; set; }

        [Required]
        [DisplayName("Menu")]
        [Index("IX_Role_Menu_Unique", 1, IsUnique = true)]
        public Guid MenuId { get; set; }

        [ForeignKey("MenuId")]
        [ToTS(TSFlag.Ignore)]
        public virtual Menu Menu { get; set; }
        
        #endregion
    }
}
