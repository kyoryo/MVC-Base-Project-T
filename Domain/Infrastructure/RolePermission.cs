﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;
using Microsoft.AspNet.Identity.EntityFramework;
using Scaffold;
using Scaffold.Models.Interface;

namespace Domain.Infrastructure
{
    [TrackChanges()]
    public class RolePermission : IModel<Guid>
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        // Skip this field from audit trail
        [SkipTracking()]
        [Required]
        [ToTS(TSFlag.SkipCRUD)]
        public DateTime CreatedAt { get; set; }

        // Skip this field from audit trail
        [SkipTracking()]
        [HiddenInput]
        [ToTS(TSFlag.SkipCRUD)]
        public DateTime? LastUpdateTime { get; set; }

        #region Relationship
        
        [Required]
        [DisplayName("Role")]
        [Index("IX_Role_Permission_Unique", 0, IsUnique = true)]
        public string RoleId { get; set; }

        [ForeignKey("RoleId")]
        [ToTS(TSFlag.Ignore)]
        public virtual ApplicationRole Role { get; set; }

        [Required]
        [DisplayName("Permission")]
        [Index("IX_Role_Permission_Unique", 1, IsUnique = true)]
        public Guid PermissionId { get; set; }

        [ForeignKey("PermissionId")]
        [ToTS(TSFlag.Ignore)]
        public virtual Permission Permission { get; set; }
        
        #endregion
    }
}
