﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Domain.Infrastructure
{
    public class CreateRoleBindingModel
    {
        [Required]
        [StringLength(256, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 2)]
        [Display(Name = "Role Name")]
        public string Name { get; set; }
        public string Description { get; set; }
    }

    public class UsersInRoleModel
    {
        public string Id { get; set; }
        public List<string> EnrolledUsers { get; set; }
        public List<string> RemovedUsers { get; set; }
    }

    public class RoleViewModel
    {
        public string Id { get; set; }
        public string RoleName { get; set; }
        public string RoleDescription { get; set; }
        public IList<RolePermission> RolePermissions { get; set; }
        public IList<RoleMenu> RoleMenus { get; set; }
    }
}